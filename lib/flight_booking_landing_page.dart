import 'package:flutter/material.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/flight_booking/bg_booking.png'),
              fit: BoxFit.cover),
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(top: 30.0, left: 10.0, right: 10.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Icon(
                    Icons.more_horiz,
                    color: Colors.white,
                  ),
                  new Icon(
                    Icons.alarm,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            new SizedBox(
              height: 50.0,
            ),
            new Text(
              "WELCOME YOU",
              style: TextStyle(
                  fontSize: 25.0, letterSpacing: 7.0, color: Colors.indigo),
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Column(
                  children: <Widget>[
                    buildCard("BOOK A FLIGHT", Colors.deepOrange, Icons.flight),
                    buildCard("SKY STAR", Colors.white, Icons.star_border),
                    buildCard("HOTLINE", Colors.white, Icons.phone),
                  ],
                ),
                new Column(
                  children: <Widget>[
                    buildCard("MANAGE BOOKING", Colors.white, Icons.book),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Container(
                          decoration: new BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/flight_booking/women.png'),
                                fit: BoxFit.cover),
                          ),
                          height: 80.0,
                          width: 180.0,
                          child: Center(
                              child: new Text(
                            "HOLIDAYS",
                            style: TextStyle(color: Colors.white),
                          ))),
                    ),
                    buildCard("FLIGHT SCHEDULE", Colors.white, Icons.schedule),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildCard(String text, Color color, IconData icon) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Container(
          color: color,
          height: 80.0,
          width: 180.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(icon),
              new Text(text),
            ],
          )),
    );
  }
}
